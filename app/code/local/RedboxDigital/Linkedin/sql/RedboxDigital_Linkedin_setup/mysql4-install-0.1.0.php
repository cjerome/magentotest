<?php
$installer = $this;
$installer->startSetup();
 
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
 
$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);
 
$setup->addAttribute('customer', 'linkedin_profile', array(
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'Linkedin Profile',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global' =>1,
    'visible_on_front'  => 1,
));
 
$setup->addAttributeToGroup(
 $entityTypeId,
 $attributeSetId,
 $attributeGroupId,
 'linkedin_profile',
 '999'  //sort_order
);
 
$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'linkedin_profile');
$oAttribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));
$oAttribute->save();
$installer->endSetup();
 